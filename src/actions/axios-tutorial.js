import axios from 'axios';
const URL = `http://110.74.194.125:3535/api/tutorials`;
export async function getAllTutorial (){
    const result = await axios({        
            method:'GET',
            headers:{
                'Content-Type':'application/x-www-form-urlencoded',
            },
            url: URL,           
           
        }).then(function (response){
            console.log(response);
        })
       .catch(function (error){
          
       });
       console.log(result);
    return {
        type:'GET_TUTORIAL',
        payload:{'data':1}
    }   
}

export async function getTutorialById (id){
    const result = await axios({        
            method:'POST',
            headers:{
                'Content-Type':'application/x-www-form-urlencoded',
            },
            url: URL+'/'+id,           
           
        }).then(function (response){
            console.log(response);
        })
       .catch(function (error){
          
       });
       console.log(result);
    return {
        type:'GET_TUTORIAL_BY_ID',
        payload:{'data':1}
    }   
}