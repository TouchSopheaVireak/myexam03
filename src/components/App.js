import React, { Component } from 'react';
import compose from 'recompose/compose';
import { connect } from 'react-redux';
import { getAllTutorial } from '../actions/axios-tutorial';
import { bindActionCreators } from 'redux';
import Tutorial from './Tutorial';
class App extends Component {
   state = {
      open: false,
      anchorEl: null,
   };
   componentWillMount(){
    this.props.getAllTutorial();
    }
   render() {
     
      return (
          <div><Tutorial/></div>
      );
  }
};
const mapStateToProps = (state) => {
    return {
        tables:state.tutorial
    };
 }
const mapDispatchToProps = (dispatch) => { 
    return bindActionCreators({
        getAllTutorial,
    },dispatch);
 }
 export default compose(
    connect(mapStateToProps,mapDispatchToProps)
  )(App);