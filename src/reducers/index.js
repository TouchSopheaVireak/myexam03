import { combineReducers } from 'redux';
import tutorial from './tutorial';
const rootReducer = combineReducers({
    tutorial
});
export default rootReducer;