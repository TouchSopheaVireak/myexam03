export default function (state={},action){
    
    switch(action.type){
        case 'GET_TUTORIAL':
            return {...state,tutorial:action.payload}
        default:
            return state
    }
}